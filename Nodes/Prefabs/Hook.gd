extends Area

const SPEED = 10
var velocity = Vector3()
var direction = 1
var isGrabbed = true
#var damage

#const shoot_type = {1: ["red_shoot", 1], 2: ["blue_shoot", 3]}
#var current_shoot = shoot_type[1]

func _physics_process(delta: float) -> void:
	if !isGrabbed :
		velocity.y = SPEED * delta * direction
	else :
		velocity.y = 0
		
	translate(velocity)
#	$AnimatedSprite.play(current_shoot[0])
	pass

#func _on_VisibilityNotifier2D_screen_exited() -> void:
#	queue_free()
#	pass # Replace with function body.

func throw():
	print ("jogou!")
	isGrabbed = false
	pass

func set_fireball_direction(dir):
	direction = dir
	if dir == -1:
		$AnimatedSprite.flip_h = true
	pass

#func _on_Hook_body_entered(body):
#	print(body.name, " entrou no collider do gancho!")
#	pass # Replace with function body.


func _on_Hook_area_shape_entered(area_id, area, area_shape, self_shape):
	print(area.name, " entrou na área do gancho!")
	if "Grabber" in area.name:
		isGrabbed = true
		print("Segurou!")
	pass # Replace with function body.
