extends KinematicBody

const MOVE_SPEED = 5
const JUMP_FORCE = 12
const GRAVITY = 9.8
const MAX_FALL_SPEED = 30

const HOOK = preload("res://Nodes/Prefabs/Hook.tscn")
const arm_type = {1: "hammer", 2: "screwdriver"}

var y_velocity = 0
var facing_right = true
var is_dead = false
var is_attacking = false
var on_ground = false

#onready var anim_player = $Graphics/AnimationPlayer


# Called when the node enters the scene tree for the first time.
func _ready():
	move_lock_z = true
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	var move_dir = 0
	if !is_dead:
		if Input.is_action_pressed("ui_right"):
#			if !is_attacking or !is_on_floor():
#				velocity.x = SPEED
#				if !is_attacking:
#					$AnimatedSprite.flip_h = false
#					$AnimatedSprite.play("run")
#					if sign($Position2D.position.x) == -1:
#						$Position2D.position.x *= -1
			move_dir += 1
		elif Input.is_action_pressed("ui_left"):
#			if !is_attacking or !is_on_floor():
#				velocity.x = -SPEED
#				if !is_attacking:
#					$AnimatedSprite.flip_h = true
#					$AnimatedSprite.play("run")
#					if sign($Position2D.position.x) == 1:
#						$Position2D.position.x *= -1
			move_dir -= 1
			
		move_and_slide(Vector3(move_dir * MOVE_SPEED, y_velocity, 0), Vector3(0,1,0))
		
		if Input.is_action_just_pressed("ui_use_hook"):
			_use_hook()
		
		on_ground = is_on_floor()
		y_velocity -= GRAVITY * delta
		if(y_velocity < -MAX_FALL_SPEED):
			y_velocity = -MAX_FALL_SPEED
			
		if on_ground:
			y_velocity = -0.1
#			if move_dir == 0:
#				play_anim("idle")
#			else:
#				play_anim("walk")
			
		if move_dir < 0 and facing_right:
			_flip()
		elif move_dir > 0 and !facing_right:
			_flip()
#		else:
#			velocity.x = 0
#			if on_ground == true and !is_attacking:
#				$AnimatedSprite.play("idle")

#		if Input.is_action_just_pressed("ui_up"):
##			velocity.y = JUMP_FORCE
##			on_ground = false
#			jump_count += 1
#			print("jump_count: %d" % jump_count)
#			if jump_count <= 2:
#				velocity.y = JUMP_FORCE
#				on_ground = false
#
#		if Input.is_action_just_pressed("ui_focus_next") and !is_attacking:
#			if is_on_floor():
#				velocity.x = 0
#			is_attacking = true
#			$AnimatedSprite.play("attack")
#			var fireball = FIREBALL.instance()
#			fireball.set_fireball_power(fireball_power)
#
#			if sign($Position2D.position.x) == 1:
#				fireball.set_fireball_direction(1)
#			else:
#				fireball.set_fireball_direction(-1)
#			get_parent().add_child(fireball)
#			fireball.position = $Position2D.global_position
#
#		velocity.y += GRAVITY
#
#		velocity = move_and_slide(velocity, FLOOR)
#
#		if is_on_floor():
#			if !on_ground:
#				on_ground = true
#				is_attacking = false
#				jump_count = 0
#		else:
#			if !is_attacking:
#				if on_ground:
#					on_ground = false
#					jump_count = 1
#				if velocity.y < 0:
#					$AnimatedSprite.play("jump")
#				else:
#					$AnimatedSprite.play("fall")
#
#		if get_slide_count() > 0:
#			for i in range(get_slide_count()):
#				if "Enemy" in get_slide_collision(i).collider.name:
#					dead()
	pass
	
func _use_hook():
	$Hook.throw()
	pass
	
func _flip():
	$Graphics.rotation_degrees.y *= -1
	facing_right = !facing_right
	pass
	
#func _play_anim(anim):
#	if anim_player.current_animation == anim:
#		return
#	anim_player.play(anim)
#	pass
	
